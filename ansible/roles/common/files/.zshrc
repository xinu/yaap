# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
ZSH_THEME="robbyrussell"

DISABLE_AUTO_UPDATE="true"

#User configuration
export PATH="/usr/local/bin:/usr/bin:~/bin:/usr/local/sbin:/usr/sbin"

source $ZSH/oh-my-zsh.sh
