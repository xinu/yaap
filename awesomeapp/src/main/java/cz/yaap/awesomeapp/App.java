package cz.yaap.awesomeapp;

import static spark.Spark.*;

import spark.ModelAndView;
import spark.servlet.SparkApplication;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.Map;

public class App implements SparkApplication {

    public void init() {
        String layout = "templates/layout.vm";

        get("/", (request, response) -> {
            Map model = new HashMap();
            model.put("template", "templates/root.vm");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        get("/:number", (request, response) -> {
            Map model = new HashMap();
            model.put("template", "templates/fib.vm");
            try {
                int number = Integer.parseInt(request.params(":number"));
                model.put("result", "Result: fib(" + number + ") = " + Fib.fib(number));
                return new ModelAndView(model, layout);
            } catch (NumberFormatException e) {
                model.put("result", "Parameter: < " + request.params(":number") + " > is not integer");
                return new ModelAndView(model, layout);
            }
        }, new VelocityTemplateEngine());
    }

    public static void main(String[] args) {
        new App().init();
    }
}
