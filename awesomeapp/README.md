# AwesomeApp README

AwesomeApp is base on SparkJava and can calculate Fibonacci sequence

### Requirements
 - any Java application server
 - JVM 1.8
 - Maven 3.x for building
 
### Building App
Jus run `mvn clean package` in awesomeapp folder
 
### Usage
after app launch go to http://localhost:8080
to calculate Fibonacci sequence for number __n__ go to http://localhost:8080/n where __n__ is an integer