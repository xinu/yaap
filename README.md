# Yet Another Awesome Project

### Requirements on local machine
- Vagrant 2.x
- Ansible 2.x
- ab (for app benchmark)

### HowTo
- to setup VBox machine run `vagrant up --provider=virtualbox`
- to deploy new version of AwesomeApp 
    - modify "awesomeapp_version" property in ansible/host_vars/app_serve1.yml
    - run `vagrant provision --provision-with deploy`
    
- open browser in https://localhost:8888/awesomeapp
    to calculate Fibonacci number use: https://localhost:8888/awesomeapp/$number
    where number is an Integer
    
- monitoring: https://localhost:8899/nagios
    username: nagiosadmin
    password: nagiosadmin
    
- for benchmark use for ex. ApacheBenchmark: `ab -c 10 -n 20000 https://localhost:8888/awesomeapp/10`